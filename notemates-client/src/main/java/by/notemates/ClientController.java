package by.notemates;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ClientController {
    @RequestMapping(value = "/", produces = MediaType.TEXT_HTML_VALUE)
    public String index(Model model)
    {
        model.addAttribute("name", "World");
//        return "index.jsp";
        return "news";
    }

}
