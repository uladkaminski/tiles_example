package by.notemates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotematesClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotematesClientApplication.class, args);
	}
}
